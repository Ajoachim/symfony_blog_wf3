<?php


namespace App\Controller\Admin;



use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\This;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController
 * @package App\Controller\Admin
 *
 * préfixe de route pour toutes les routes définies dans cette classe
 * @Route("/categorie")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(CategoryRepository $repository)
    {
        //sans injecter le repository:
       // $repo = $this->getDoctrine()->getRepository(CategoryRepository::class);
      //$categories = $repository->findAll();

      //findAll() avec un tri sur le nom:
      $categories = $repository->findBy([], ['name' => 'ASC']);

        return $this->render(
            'admin/category/index.html.twig',[
                'categories' => $categories
            ]
        );

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/edition/{id}", defaults={"id" : null}, requirements={"id" : "\d+"})
     */
    public function edit(Request $request, EntityManagerInterface $em, $id)
    {
        if (is_null($id)){//création
            $category = new Category();
        }else{//modification
        $category = $em->find(Category::class, $id);

        //404 si l'id n'est pas en bdd
        if (is_null($category)){
            throw new NotFoundHttpException();
        }
        }
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        //le formulaire analyse la requête et fait le mapping
        $form->handleRequest($request);

        if ($form->isSubmitted() ) {
            if ($form->isValid()){
                $em->persist($category);
                $em->flush();

                $this->addFlash('success', 'La catégorie est enregistrée');
                return $this->redirectToRoute('app_admin_category_index');
            }else{
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
            //$em = $this->getDoctrine()->getManager();

        }

        return $this->render('admin/category/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     *ParamConverter : $category est un objet Category correspondant à la catégorie dont on a reçu l'id dans l'url
     *
     * @Route("/suppression/{id}")
     */
    public function delete(Category $category, EntityManagerInterface $em)
    {
        $name = $category->getName();
        $em->remove($category);
        $em->flush();

        $this->addFlash('succes', "La catégorie $name est supprimée");

        return $this->redirectToRoute('app_admin_category_index');
    }
}